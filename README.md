# Learning OpenGL with Python

This repository holds code associated with the blog posts on metamost.com

* [Tutorial 1: Window](https://metamost.com/opengl-with-python)
* [Tutorial 2: First Triangle](https://metamost.com/opengl-with-python)
